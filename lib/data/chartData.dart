import 'package:firebase_database/firebase_database.dart';

class Note {
  final String Date;
  final String Type;
  final String Detail;
  final int Price;

  Note({required this.Detail, required this.Date, required this.Type, required this.Price});

  factory Note.fromRTDB(Map<String, dynamic> data) {
    return Note(
      Detail: data['Detail'] ?? "",
      Date: data['Date'] ?? "",
      Type: data['Type'] ?? "",
      Price: data['Price'] ?? 0
    );
  }

  
}