import 'package:flutter/material.dart';
import 'package:project_aj_kob/pages/calendarPage.dart';
import 'package:project_aj_kob/pages/addDataPage.dart';
import 'package:project_aj_kob/pages/homePage.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget { 
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Project Term',
      theme: new ThemeData(scaffoldBackgroundColor: const Color(0xFFEFEFEF)),
      home: MyHomePage(),
    );
  }
} 

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  PageController pageController = PageController();
  List<Widget> pages = [HomePage(),calendarPage(),AddDataPage()];

  int selectIndex = 0;
  void onPageChanged(int index) {
    setState(() {
      selectIndex = index;
    });
  }

  void onTapItem(int selectedItems) {
    pageController.jumpToPage(selectedItems);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        controller: pageController,
        children: pages,
        onPageChanged: onPageChanged,
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.blue,
        onTap: onTapItem,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home,color: selectIndex==0?Colors.black:Colors.white,),
            title: Text("Home", style: TextStyle(color: selectIndex==0?Colors.black:Colors.white,),)
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.calendar_today,color: selectIndex==1?Colors.black:Colors.white,),
            title: Text("Calendar", style: TextStyle(color: selectIndex==0?Colors.black:Colors.white,),)
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.note_add,color: selectIndex==2?Colors.black:Colors.white,),
            title: Text("Note", style: TextStyle(color: selectIndex==0?Colors.black:Colors.white,),)
          ),
        ],
      ),
    );
  }
}
