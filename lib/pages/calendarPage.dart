import 'dart:js';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:project_aj_kob/data/chartData.dart';
import 'package:project_aj_kob/pages/detailPage.dart';
import 'package:table_calendar/table_calendar.dart';

class calendarPage extends StatefulWidget {
  const calendarPage({Key? key}) : super(key: key);

  @override
  _calendarPageState createState() => _calendarPageState();
}

class _calendarPageState extends State<calendarPage> {
  CalendarFormat _calendarFormat = CalendarFormat.month;
  DateTime _focusedDay = DateTime.now();
  DateTime? _selectedDay;
  String dateString = '';
  String formattedDate = '';
  List<Note> noteData = [];
  bool isVisible = true;
  final Stream<QuerySnapshot> _noteList =
      FirebaseFirestore.instance.collection('Note').snapshots();
  CollectionReference notes = FirebaseFirestore.instance.collection('Note');
  Future<void> delNote(noteId) {
    return notes.doc(noteId).delete().then((value) => print('Note Delete'));
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Table Calendar"),
        ),
        backgroundColor: Colors.blue,
        body: Column(
          children: <Widget>[
            Center(
              child: Padding(
                padding: const EdgeInsets.all(15),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.white,
                  ),
                  child: TableCalendar<Widget>(
                    locale: 'en_US',
                    firstDay: DateTime.utc(2010, 10, 16),
                    lastDay: DateTime.utc(2030, 3, 14),
                    focusedDay: DateTime.now(),
                    selectedDayPredicate: (day) {
                      return isSameDay(_selectedDay, day);
                    },
                    onDaySelected: (selectedDay, focusedDay) {
                      if (!isSameDay(_selectedDay, selectedDay)) {
                        setState(() {
                          _selectedDay = selectedDay;
                          formattedDate =
                              DateFormat('yyyy-MM-dd').format(_selectedDay!);
                          print(_selectedDay);
                          print(formattedDate);
                          _focusedDay = focusedDay;
                        });
                      }
                    },
                    onFormatChanged: (format) {
                      if (_calendarFormat != format) {
                        setState(() {
                          _calendarFormat = format;
                        });
                      }
                    },
                    onPageChanged: (focusedDay) {
                      _focusedDay = focusedDay;
                    },
                  ),
                ),
              ),
            ),
            Expanded(
                child: SizedBox(
              height: 80,
              child: StreamBuilder<QuerySnapshot>(
                stream: _noteList,
                builder: (BuildContext context,
                    AsyncSnapshot<QuerySnapshot> snapshot) {
                  if (snapshot.hasError) {
                    return Text('Something went wrong');
                  }

                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return Text("Loading");
                  }
                  return ListView(
                    children:
                        snapshot.data!.docs.map((DocumentSnapshot document) {
                      Map<String, dynamic> data =
                          document.data()! as Map<String, dynamic>;

                      return Visibility(
                        visible: formattedDate != data['Date']
                            ? isVisible = false
                            : isVisible = true,
                        child: Padding(
                          padding: const EdgeInsets.all(15),
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: Colors.orangeAccent,
                            ),
                            child: ListTile(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => detailPage(
                                              noteId: document.id,
                                            )));
                              },
                              title: Text(data['Type']),
                              subtitle: Text(
                                  '${data['Detail']}        Price : ${data['Price']}'),
                              trailing: IconButton(
                                icon: Icon(Icons.delete),
                                onPressed: () {
                                  delNote(document.id);
                                },
                              ),
                            ),
                          ),
                        ),
                      );
                    }).toList(),
                  );
                },
              ),
            ))
          ],
        ));
  }
}
