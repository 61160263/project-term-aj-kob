import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class AddDataPage extends StatefulWidget {
  const AddDataPage({Key? key}) : super(key: key);

  @override
  _AddDataPageState createState() => _AddDataPageState();
}

class _AddDataPageState extends State<AddDataPage> {
  CollectionReference note = FirebaseFirestore.instance.collection('Note');
  final List<String> entries = <String>['Food', 'Travel', 'Other'];
  final typeController = TextEditingController();
  final priceController = TextEditingController();
  final detaiController = TextEditingController();

  Future<void> addNote() {
    DateTime now = DateTime.now();
    String formattedDate = DateFormat('yyyy-MM-dd').format(now);
    return note.add({
      'Date': formattedDate.toString(),
      'Type': typeController.text,
      'Detail': detaiController.text,
      'Price': priceController.text
    });
  }

  @override
  Widget build(BuildContext context) {
    var time;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("Note"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(15, 15, 15, 0),
              child: Text(
                'Type',
                style: TextStyle(
                  fontFamily: 'SF Pro',
                  fontWeight: FontWeight.w700,
                  fontSize: 20.0,
                ),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
              child: TextFormField(
                controller: typeController,
                decoration: InputDecoration(
                  fillColor: Colors.white,
                  hintText: 'Enter type...',
                  hintStyle: TextStyle(color: Colors.grey),
                  filled: true,
                  border: UnderlineInputBorder(),
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please input type';
                  }
                  return null;
                },
                style: TextStyle(color: Colors.black),
                keyboardType: TextInputType.text,
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(15, 15, 15, 0),
              child: Text(
                'Detail',
                style: TextStyle(
                  fontFamily: 'SF Pro',
                  fontWeight: FontWeight.w700,
                  fontSize: 20.0,
                ),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
              child: TextFormField(
                controller: detaiController,
                decoration: InputDecoration(
                  fillColor: Colors.white,
                  hintText: 'Enter detai...',
                  hintStyle: TextStyle(color: Colors.grey),
                  filled: true,
                  border: UnderlineInputBorder(),
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please input detail';
                  }
                  return null;
                },
                style: TextStyle(color: Colors.black),
                keyboardType: TextInputType.text,
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(15, 15, 15, 0),
              child: Text(
                'Price',
                style: TextStyle(
                  fontFamily: 'SF Pro',
                  fontWeight: FontWeight.w700,
                  fontSize: 20.0,
                ),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
              child: TextFormField(
                controller: priceController,
                decoration: InputDecoration(
                  fillColor: Colors.white,
                  hintText: 'Enter price...',
                  hintStyle: TextStyle(color: Colors.grey),
                  filled: true,
                  border: UnderlineInputBorder(),
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please input price';
                  }
                  return null;
                },
                style: TextStyle(color: Colors.black),
                keyboardType: TextInputType.text,
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(15, 15, 15, 15),
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(),
                  child: Text('Save'),
                  onPressed: () async {
                    
                    await addNote();
                    typeController.clear();
                    detaiController.clear();
                    priceController.clear();
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
