import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class detailPage extends StatefulWidget {
  String noteId;

  detailPage({Key? key, required this.noteId}) : super(key: key);

  @override
  _detailPageState createState() => _detailPageState(this.noteId);
}

class _detailPageState extends State<detailPage> {
  String noteId;
  String type = '';
  String detail = '';
  String price = '';

  TextEditingController detailController = new TextEditingController();
  TextEditingController typeController = new TextEditingController();
  TextEditingController priceController = new TextEditingController();
  int s = 0;
  _detailPageState(this.noteId);
  final _formKey = GlobalKey<FormState>();
  CollectionReference notes = FirebaseFirestore.instance.collection('Note');
  
  @override
  void initState() {
    super.initState();
    if (this.noteId.isNotEmpty) {
      notes.doc(this.noteId).get().then((snapshot) {
        if (snapshot.exists) {
          var data = snapshot.data() as Map<String, dynamic>;
          type = data['Type'];
          detail = data['Detail'];
          price = data['Price'];
          typeController.text = type;
          detailController.text = detail;
          priceController.text = price;
        }
      });
    }
  }

  Future<void> addUser() {
    return notes
        .add({'Type': this.type, 'Detail': this.detail, 'Price': this.price})
        .then((value) => print("User Added"))
        .catchError((error) => print("Failed to add user: $error"));
  }

  Future<void> updateNote() {
    return notes.doc(this.noteId).update({
      'Type': this.type,
      'Detail': this.detail,
      'Price': this.price
    }).then((value) => print("update"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("Edit"),
      ),
      body: Form(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(15, 15, 15, 0),
              child: Text(
                'Type',
                style: TextStyle(
                  fontFamily: 'SF Pro',
                  fontWeight: FontWeight.w700,
                  fontSize: 20.0,
                ),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
              child: TextFormField(
                controller: typeController,
                decoration: InputDecoration(
                  fillColor: Colors.white,
                  hintText: 'Enter type...',
                  hintStyle: TextStyle(color: Colors.grey),
                  filled: true,
                  border: UnderlineInputBorder(),
                ),
                onChanged: (value) {
                  setState(() {
                    type = value;
                  });
                },
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please input type';
                  }
                  return null;
                },
                style: TextStyle(color: Colors.black),
                keyboardType: TextInputType.text,
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(15, 15, 15, 0),
              child: Text(
                'Detail',
                style: TextStyle(
                  fontFamily: 'SF Pro',
                  fontWeight: FontWeight.w700,
                  fontSize: 20.0,
                ),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
              child: TextFormField(
                controller: detailController,
                decoration: InputDecoration(
                  fillColor: Colors.white,
                  hintText: 'Enter detai...',
                  hintStyle: TextStyle(color: Colors.grey),
                  filled: true,
                  border: UnderlineInputBorder(),
                ),
                onChanged: (value) {
                  setState(() {
                    detail = value;
                  });
                },
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please input detail';
                  }
                  return null;
                },
                style: TextStyle(color: Colors.black),
                keyboardType: TextInputType.text,
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(15, 15, 15, 0),
              child: Text(
                'Price',
                style: TextStyle(
                  fontFamily: 'SF Pro',
                  fontWeight: FontWeight.w700,
                  fontSize: 20.0,
                ),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
              child: TextFormField(
                controller: priceController,
                decoration: InputDecoration(
                  fillColor: Colors.white,
                  hintText: 'Enter price...',
                  hintStyle: TextStyle(color: Colors.grey),
                  filled: true,
                  border: UnderlineInputBorder(),
                ),
                onChanged: (value) {
                  setState(() {
                    price = value;
                  });
                },
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please input price';
                  }
                  return null;
                },
                style: TextStyle(color: Colors.black),
                keyboardType: TextInputType.text,
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(15, 15, 15, 15),
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(),
                  child: Text('Save'),
                  onPressed: () async {
                    await updateNote();

                    Navigator.pop(context);
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
