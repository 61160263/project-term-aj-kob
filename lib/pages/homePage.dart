import 'package:flutter/material.dart';
import 'package:project_aj_kob/data/chartData.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late List<Note> _chartData;
  DateTime now = DateTime.now();
  @override
  void initState() {
    _chartData = getChartData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.blue,
        appBar: AppBar(
          title: Text("Daily Cost"),
        ),
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(15),
              child: Container(
                padding: EdgeInsets.only(top: 8.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.orangeAccent,
                ),
                child: Column(
                  children: [
                    SizedBox(
                      child: CircleAvatar(
                        radius: 100,
                        backgroundColor: Colors.greenAccent,
                        backgroundImage: NetworkImage(
                          'https://firebasestorage.googleapis.com/v0/b/projectexample-f93fe.appspot.com/o/Proflie.jpg?alt=media&token=72edf838-95d1-4056-a35c-721b52d28804',
                        ),
                      ),
                    ),
                    Center(
                      child: Padding(
                        padding: const EdgeInsets.all(15),
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: Colors.redAccent,
                          ),
                          padding: const EdgeInsets.all(15),
                          child: Text(
                            'Hi Piyachat Sangngam',
                            style: TextStyle(
                              fontFamily: 'SF Pro',
                              fontWeight: FontWeight.w700,
                              fontSize: 24.0,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Center(
              child: Padding(
                padding: const EdgeInsets.all(15),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(20),
                  child: SfCircularChart(
                    backgroundColor: Colors.orangeAccent,
                    legend: Legend(
                        isVisible: true,
                        overflowMode: LegendItemOverflowMode.wrap),
                    series: <CircularSeries>[
                      DoughnutSeries<Note, String>(
                          dataSource: _chartData,
                          xValueMapper: (Note data, _) => data.Type,
                          yValueMapper: (Note data, _) => data.Price,
                          dataLabelSettings: DataLabelSettings(isVisible: true))
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  List<Note> getChartData() {
    final List<Note> data = [
      Note(Date: '2019-10-29', Detail: '', Type: 'Food', Price: 400),
      Note(Date: '2019-10-29', Detail: '', Type: 'Travel', Price: 150),
      Note(Date: '2019-10-29', Detail: '', Type: 'Other', Price: 100),
    ];
    return data;
  }
}
